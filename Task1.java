// variant 6
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Task1 {

  public static void main(String[] args) {
    List<Cashier> cashiers = new LinkedList<>();
    List<Thread> cashiersThread = new LinkedList<>();
    ConcurrentLinkedQueue<Customer> customers = new ConcurrentLinkedQueue<>();

    for (int i = 0; i < 3; i++) {
      Cashier cashier = new Cashier(i, customers);
      cashiers.add(cashier);
      Thread thread = new Thread(cashier);
      cashiersThread.add(thread);
    }

    for (int i = 0; i < 10; i++) {
      int t = 1 + (int) (9 * Math.random());
      Customer customer = new Customer("Customer " + i, t);
      customers.add(customer);
    }

    for (int i = 0; i < 3; i++) {
      cashiersThread.get(i).start();
    }

    while (!customers.isEmpty()) {}

    System.out.println("Haven't customers");
  }

}

class Cashier implements Runnable {


  int id;
  volatile private ConcurrentLinkedQueue<Customer> customerQueue;

  public Cashier(int id, ConcurrentLinkedQueue<Customer> customerQueue) {
    this.id = id;
    this.customerQueue = customerQueue;
  }

  @Override
  public void run() {
    while (true) {
      try {
        Customer currentCustomer;
        while (customerQueue.size() == 0) {}
        currentCustomer = customerQueue.poll();
        System.out.println(this + " have customer " + currentCustomer);
        Thread.sleep(500L * currentCustomer.getTaskQty());
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public String toString() {
    return String.valueOf(id);
  }
}

class Customer {

  private String name;
  private int taskQty;

  public Customer(String name, int taskQty) {
    this.name = name;
    this.taskQty = taskQty;
  }

  public int getTaskQty() {
    return taskQty;
  }

  @Override
  public String toString() {
    return name;
  }
}